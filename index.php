<?php

require("./init.php");
?>

<!DOCTYPE html>
<html lang= "en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>  
    <LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <LINK rel="stylesheet" type="text/css" href="assets/style.css">
<body>

	<?php require("div/nav.php"); ?>

  <!-- start body-->
  <div class="container">
    <!-- CAROUSEL  CODE -->
    <div class="col-md-8" id="sidebar">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="http://placehold.it/800x340" alt="...">
      <div class="carousel-caption">
        HELLO IS IT ME YOURE LOOKING FOR
      </div>
    </div>
    <div class="item">
      <img src="http://placehold.it/800x340" alt="...">
      <div class="carousel-caption">
        I CAN SEE IT IN YOUR EYES
      </div>
    </div>
    <div class="item">
      <img src="http://placehold.it/800x340" alt="...">
      <div class="carousel-caption">
        I CAN SEE IT IN YOUR SMILE
      </div>
    </div>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<!-- END CAROUSEL CODE -->
  <div class="row" id="main-content">
  	<div class="col-md-4" id="sidebar">
  		<div class="well">
  			<form id ="loginForm" method="POST" action="/login/"
  			novalidate="novalidate">
  				<fieldset>
  					<legend>Access Your Account</legend>
  					<input type="text" id="userName" class="form-control chat-input" name="username" value="" 
  					required="" title="Please enter your username"
  					placeholder="User Email">
  				</br>
  					<input type="text" id="userPassword"
  					class="form-control chat-input" name="password"
  					value="" required="" title="Please enter your password"
  					placeholder="Password">
  				<div class="pull-right">
  				<input type="checkbox" name="remember" id="remember"> Remember Me
  			</div>
  					<div class="form-group">
  					<input type="submit" class="btn btn-primary btn-block" value="Login">
  				</br>
  					<a href="#register" role= "button" data-toggle="modal" data-target="#modal-1" class="btn btn-info btn-block">New User? Register Now!</a>
  				</br>
  				</div>

  				</fieldset>

  			</form>
  		</div>
  	</div>

  </div>
  <!-- THE CIRCLES OF LIFE-->
 <div class="row  pad-row text-center" id="3c">
               
                <div class="col-md-4 col-sm-4 col-xs-4">
                     
                    <img src="http://placehold.it/420x420" class="img-responsive img-circle" alt="..."/>
                     <h4>Internet</h4>
                    <p>
                        Blazing speeds
                    </p>
                    <a href="#" class="btn btn-primary" >Read Details</a>
                    </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                     
                    <img src="http://placehold.it/420x420" class="img-responsive img-circle" alt="..."/>
                     <h4>Wired Phone</h4>
                    <p>
                        Wires
                    </p>
                    <a href="#" class="btn btn-primary" >Read Details</a>
                    </div>
             <div class="col-md-4 col-sm-4 col-xs-4">
                     
                    <img src="http://placehold.it/420x420" class="img-responsive img-circle" alt="..."/>
                  <h4>Wireless Phone</h4>
                    <p>
                        No wires
                    </p>
                    <a href="#" class="btn btn-primary" >Read Details</a>
                    </div>
             
                
        </div>
        <br><br><br><br><br>

  <!-- end body-->

  <!--following is footer-->
  <div class = "navbar navbar-default navbar-fixed-bottom">
    <div class = "container">
      <p class = "navbar-text">Site Powered by <b>Byte My Ascii</b></p>
    </div> 
            </div>
  <!--end footer-->
</div><!-- end of container-->


<!-- MODAL FOR REGISTERING -->
<div class="modal fade" id="modal-1" tabindex="-1" aria-labeledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"> &times; </button>
				<h3 id="myModalLabel"class="modal-title"> Register Now!</h3>
			</div>
			<div class="modal-body">
				<form id ="registerForm" method="POST" action="/register/"
  			novalidate="novalidate">
  				<fieldset>
 				
  					<input type="text" id="firstName" class="form-control chat-input"
  					name= "personName" value="" required="" title="Please enter your first Name" placeholder="First Name">
  				</br>
   					<input type="text" id="lastName" class="form-control chat-input"
  					name= "personName" value="" required="" title="Please enter your last Name" placeholder="Last Name"> 						
  				</br>
  					<input type="text" id="userNameReg" class="form-control chat-input" name="username" value="" 
  					required="" title="Please enter your username"
  					placeholder="User Email">
  				</br>
  					<input type="text" id="userPasswordReg"
  					class="form-control chat-input" name="password"
  					value="" required="" title="Please enter your password"
  					placeholder="Password">
  				</fieldset>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-info" data-dismiss="modal" aria-hidden="true"> Cancel Registration</button>
				<button class="btn btn-success">Register</button>
			</div>
		</div>
	</div>
	</div>
</div>

<!-- END OF MODAL FOR REGISTERING -->


  <!--Scripts-->
	<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
	<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>

</body>
</html>