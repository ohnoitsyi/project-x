# Project X


To provide the ecommerce web application for __Telecom Company__   
Brought to you by __Byte My Ascii__

Name: Project X   
Version: 1.0

## Feature Included

* Sign up Retail Customer for an account
* Securely login to account
* Add a service
* Delete a service
* Add a package
* Add service to package (for Telegroup marketing reps only)
* View online Bill
* Sign up Commercial Customer for services
* Cancel Services

[Project Information](https://sites.google.com/a/eng.ucsd.edu/cse110win2015/project/outline)

