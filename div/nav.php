<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">TelePort LLC</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo URL; ?>">Home</a></li>
        <li class= "dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">MarketPlace
          <span class="caret"></span></a>
          <ul class = "dropdown-menu">
            <li><?php echo a($GLOBALS["urls"]['marketplace'], "All Services"); ?></li>
            <li><?php echo a($GLOBALS["urls"]['marketplace'] . "?filter=wiredPhone", "Wired Phone"); ?></li>
            <li><?php echo a($GLOBALS["urls"]['marketplace'] . "?filter=wirelessPhone", "Wireless Phone"); ?></li>
            <li><?php echo a($GLOBALS["urls"]['marketplace'] . "?filter=internet", "Internet"); ?></li>
          </ul>
        </li>
      </ul>
      <?php if(!Usermanager::isLogin()){ ?>
      <ul class="nav navbar-nav navbar-right">
        <li><?php echo a($GLOBALS["urls"]["signup"], '<span class="glyphicon glyphicon-user"></span> Sign Up</a>', false); ?></li>
        <li><?php echo a($GLOBALS["urls"]["login"], '<span class="glyphicon glyphicon-log-in"></span> Login</a>', false); ?></li>
        <li><?php //echo a("Account/index.php?do=logout", '<span class="glyphicon glyphicon-log-in"></span> Logout</a>'); ?></li>
      </ul>
      <?php } else { ?>
      <ul class="nav navbar-nav navbar-right">
        <li class= "dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Hello, <?php echo $_SESSION['user']['name']; ?>
          <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?php echo $GLOBALS['urls']['profile'] ?>">ID: <?php echo md5($_SESSION['user']['email']); ?></a></li>
            <li><a href="#">Email: <?php echo $_SESSION['user']['email']; ?></a></li>
            <li><a href="#">Role: <?php echo $_SESSION['user']['role']; ?></a></li>
            <li class="divider"></li>
            <li><a href="<?php echo $GLOBALS['urls']['change_password'] ?>">Change Password</a></li>
          </ul>
        </li>
        <li><a href="<?php echo $GLOBALS['urls']['logout'] ?>"><span class="glyphicon glyphicon-log-in"></span> Log Out</a></li>
      </ul>
      <?php } ?>
    </div>
  </div>
</nav>

