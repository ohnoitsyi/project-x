<?php

define("ROOT", "/Applications/XAMPP/htdocs/ProjectX/");
require(ROOT . "init.php");
init();
?>
<!DOCTYPE html>
<HTML>
	<HEAD>
		<META charset="utf-8">
		<TITLE>User Profile</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<BODY>
		<?php require("./div/user_nav.php"); ?>
		<section id="main" class="container">
			<h1>Welcome to Project X</h1>
			<div>See what we offer</div>
		</section>
		<footer>
			Brought to you by <b>Byte My Ascii</b>
		</footer>
		<style>
			#main { min-height: 300px; }
			footer{ border-top: 1px solid #eee; color: #111; text-align: right; padding: 30px;  }
		</style>
	</BODY>
</HTML>
