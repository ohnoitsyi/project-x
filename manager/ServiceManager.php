<?php

class ServiceManager{
	// $retail = 1 mean it is retail
	static function add($name, $description, $type, $rate, $duration, $retail){
		$str = "INSERT INTO `services`(`name`, `description`, `type`, `rate`, `duration`, `retail`) VALUES (?,?,?,?,?,?)";						
		$input = array($name, $description, $type, $rate, $duration, $retail);
		DB::run_exec($str, $input);
	}
	static function listAll(){
		$str = "SELECT id, name, description, type, rate, duration FROM services";
		return DB::run_query($str);
	}
	static function delete($service){
		$str = "DELETE FROM services WHERE id=?";
		DB::run_exec($str, array($service));
	}
	static function listOnlyRetail(){
		$str = "SELECT id, name, description, type, rate, duration FROM services WHERE retail=1";
		return DB::run_query($str);
	}
}
?>
	