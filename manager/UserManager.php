<?php

class UserManager{
	
	static function login($email, $password){ // May be static
		$input = array($email, md5($password));
		$str = "SELECT name, email, role FROM users WHERE email=? AND password=?";
		$result = DB::run_query($str, $input);
		if ($result == Array()){
			$_SESSION["msg"] = array("str" => "Invalid Email or Password.", "status" => 1);
			redirect($GLOBALS['urls']['login']);
		} else {
			// Successfully Login
			$_SESSION['user']['login'] = 1;
			print_r($result);
			$_SESSION['user']['name'] = $result[0]['name'];
			$_SESSION['user']['email'] = $result[0]['email'];
			$_SESSION['user']['role'] = $result[0]['role'];
			redirect($GLOBALS['urls']['profile']);
		}
	}

	static function logout(){
		session_unset();
		session_destroy();
		session_start();
		$_SESSION["msg"] = array("str" => "Successfully Logout!", "status" => 0);
		redirect(URL, false);
	}

	static function signup($email, $username, $password, $role = "Customer"){
		$str = "SELECT email from users WHERE email=?";
		$query = DB::run_query( $str, array($email) );
		if ($query != Array()){
			$_SESSION["msg"] = array("str" => "User already exists.", "status" => 1);
			displayMsg();
			redirect($GLOBALS['urls']['signup']);
		} else {
			$str = "INSERT INTO `users`(`email`, `name`, `password`, `role`) VALUES (?,?,?,?)";
			DB::run_exec( $str, array($email, $username, md5($password), $role) );
			$_SESSION["msg"] = array("str" => "Successfully create your account with email:" . $email, "status" => 0);
			redirect($GLOBALS['urls']['profile']);
		}
	}

	// NOT YET IMPLEMENTED
	static function changePassword(){
		if ( isset($_POST['email']) && ($_POST['email'] != "") && 
			isset($_POST['oldPassword']) && ($_POST['oldPassword'] != "") &&
			isset($_POST['newPassword']) && ($_POST['newPassword']) != ""){

			// ===
			$input = array($_POST['email'], md5($_POST['oldPassword']));
			// DB Connect
			$query = $this->DB->prepare("SELECT name, email FROM users WHERE email=? AND password=?");
			$result = $query->execute($input);
			//$result = query('SELECT name, email FROM users WHERE ');
			$result = $query->fetch(); //PDO::FETCH_ASSOC);
			
			if (!$result){
				// 
				//$_SESSION['errMsg'] = 'ERROR: Invalid Email or Password. <a href="/ProjectX/account.php?do=login">Try again</a>';
				$_SESSION["msg"] = array("str" => "Invalid Email or Password.", "status" => 1);
				//header("Location: account.php?do=reset_password");
				//exit();
				redirect("Account/?do=reset_password");
			} else {
				$query = $this->DB->prepare("UPDATE users SET password=? WHERE email=?");
				$query->execute(array( md5($_POST['newPassword']), $_POST['email']));	
				//header("Location: profile.php");
				//exit();
				redirect("Dashboard/");	
			}

			// ===
		} else {
			$_SESSION["msg"] = array("str" => 'ERROR: Incomplete form. Something is missing.', "status" => 1);
			//header("Location: account.php?do=reset_password&for=not_complete");
			//exit();
			redirect("Account/?do=reset_password&for=not_complete");
		}
	}

	static function forgetPassword(){
		// will implement later
	}


	static function isLogin(){
		if ( isset($_SESSION['user']['login']) && ($_SESSION['user']['login'] ==1)
			&& isset($_SESSION['user']['email']) && ($_SESSION['user']['email'] != "")
		 ){
			return true;
		} else {
			return false;
		}
	}

	static function listAll(){
		$result = DB::run_query('SELECT email, name, role FROM `users`');
		return $result; // ORDER BY name 
		/*
		$result = '<section class="container">'; 
		$result .= '<h1>Manage :: Users</h1>';
		$result .= '<table class="table">';
		$result .= '<tr> <th>Name</th> <th>Email</th> <th>Role</th> <th>Configure</th> </tr>';
		foreach ($users as $user){
			$result .= "<tr> <th>" . $user['name'] . "</th> <th>" 
						. $user['email'] . "</th> <th>" . $user['role'] 
						//. '</th> <th><a href="/Dashboard/Admin/?do=edit&email=' . $user['email'] .'">Change</a></th> </tr>';
				. '</th> <th>' . a("/Dashboard/Admin/?do=edit&email=" . $user['email'], "Change") . '</th> </tr>';
		}
		$result .= '</table></section>';
		return $result;
		*/
	}

	static function listCustomer(){
		$result = DB::run_query('SELECT email, name, role FROM `users` WHERE role=? OR role=?', array($GLOBALS['user_roles']['Customer'], $GLOBALS['user_roles']['RetailCustomer']));
		//print_r($result);
		return $result;
	}
}

?>