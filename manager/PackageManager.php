<?php

class Packagemanager{
	static function add($name, $description, $services, $rate){
		$DB = new DB();
		$str = "INSERT INTO `packages`(`name`, `description`, `services`, `rate`) VALUES (?,?,?,?)";						
		$input = array($name, $description, serialize($services), $rate);
		$DB->exec($str, $input);
	}

	static function edit($id, $name, $description, $services, $rate){
		// "UPDATE users SET `name`=?, `role`=? WHERE email=?"
		$DB = new DB();
		$str = "UPDATE `packages` SET `name`=?, `description`=?, `services`=?, `rate`=? WHERE `id`=?";						
		$input = array($name, $description, serialize($services), $rate, $id);
		$DB->exec($str, $input);
	}

	static function listAll(){
		$str = "SELECT id, name, description, rate FROM packages";
		return DB::run_query($str);
	}
	static function delete($package){
		$DB = new DB();
		$str = "DELETE FROM packages WHERE id=?";
		//$what = array($services->getID());
		$DB->exec($str, array($package));
	}
}

?>