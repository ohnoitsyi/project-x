<?php
	require("../init.php");
	if(Usermanager::isLogin()){ redirect($GLOBALS['urls']['profile']); }
	if (isset($_POST['email']) && isset($_POST['password'])){
		UserManager::login($_POST['email'], $_POST['password']);
	}
?>


<!DOCTYPE html>
<HTML>
	<HEAD>
		<META charset="utf-8">
		<TITLE>Account :: ProjectX</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<BODY class="container">
		<?php require(ROOT . "div/nav.php"); ?>
		<section id="login" class="box">
			<h1 class="center">Log in</h1>
			<?php echo displayMsg(); ?>
			<form method="post" class="form">
				<div class="form-group">
					<label for="email">Email : </label>
					<input type="email" name="email" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="password">Password : </label>
					<input type="password" name="password" class="form-control" required>
				</div>
				<a href="<?php echo $GLOBALS["urls"]["forget_password"] ; ?>">Forget password ?</a> <br>
				<a href="<?php echo $GLOBALS["urls"]["signup"] ; ?>">Don't have an account ? &laquo; Sign Up &raquo;</a>
				<button type="submit" class="btn btn-default pull-right" name="login" value="1">Login</button>
			</form>
		</section>

		<style>
			.box{ width: 70%; margin: 60px auto;}
			ul.list-inline { text-align: center; }	
			li a.btn { font-size: 36px; }		
		</style>
	</BODY>
</HTML>