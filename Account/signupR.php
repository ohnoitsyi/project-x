<?php
	require("../init.php");

	if ( isset($_POST['username']) && ($_POST['username'] != "") &&  
			isset($_POST['email']) && ($_POST['email'] != "") && 
			isset($_POST['password']) && ($_POST['password']) != ""){
		UserManager::signup($_POST['email'], $_POST['username'], $_POST['password'], $GLOBALS['user_roles']['RetailCustomer']);
	}
	
?>
<!DOCTYPE html>
<HTML>
	<HEAD>
		<META charset="utf-8">
		<TITLE>Account :: ProjectX</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<BODY class="container">
		<?php require(ROOT . "div/nav.php"); ?>

		<section id="signup" class="box">
		<h1 class="center">Sign Up as Retail Customer</h1>
		<p>Keep in mind that Retail Customer can't subscribe Commertial Services.</p>
		<p>Change to signup as <a href="<?php echo $GLOBALS['urls']['signup'] ?>">Commercial Customer</a> ?</p>
		<?php echo displayMsg(); ?>
		<form method="post" class="form">
			<div class="form-group">
				<label for="email">Email : </label>
				<input type="email" name="email" class="form-control" required>
			</div>
			<div class="form-group">
				<label for="username">Username : </label>
				<input type="text" name="username" class="form-control" required>
			</div>
			<div class="form-group">
				<label for="password">Password : </label>
				<input type="password" name="password" class="form-control" required>
			</div>
			<button type="submit" class="btn btn-default pull-right" name="signup" value="1">Join Us</button>
		</form>
	</section>
	<style>
			.box{ width: 70%; margin: 60px auto;}
			ul.list-inline { text-align: center; }	
			li a.btn { font-size: 36px; }		
		</style>
	</BODY>
</HTML>