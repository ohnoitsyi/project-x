<?php

function display( $str, $error = false){
	if ($error){
		return '<div class="alert alert-danger">' . $str . '</div>';
	} else {
		return '<div class="alert alert-success">' . $str . '</div>';
	}
} 


function displayMsg(){
	if (isset($_SESSION['msg']) && $_SESSION['msg'] != array()){
		if ($_SESSION['msg']['status']){
			$result = "<div class=\"alert alert-danger text-center\">{$_SESSION['msg']['str']}</div>";
		} else{
			$result = "<div class=\"alert alert-success text-center\">{$_SESSION['msg']['str']}</div>";
		} 	
		$_SESSION['msg'] = array();
		return $result;
	}
}

function redirect($url){
	header("Location: " . $url);
	exit();
}

function a($url, $title, $class = "", $id = ""){
	$str = '<a href="' . $url . '" class="'  . $class . '" id="' . $id  . '">' . $title . '</a>';
	return $str;
}



?>