<?php

define("URL","http://localhost/ProjectX/“);

//DB
define("DB_HOST", "localhost");
define("DB_NAME", "teleport");
define("DB_USER", "root");
define("DB_PASSWORD", "");


// Tele Port
$USER_ROLES = array("Customer" => "Customer", 
			"MarketingRep" => "Marketing Representative", 
			"Admin" => "Adminstrator",
			"RetailCustomer" => "Retail Customer",
			"CustomerRep" => "Cutomer Representative");

$URLS = array(
	"404" => URL . "404.php",
	"setThreshold" => URL . "Dashboard/setThreshold.php",
	"payBill" => URL . "Dashboard/payBill.php",
	"admin" => URL . "Dashboard/Admin/index.php",
	"signup" => URL . "Account/signup.php",
	"signupR" => URL . "Account/signupR.php",
	"login" => URL . "Account/login.php",
	"loginAs" => URL . "Dashboard/loginAs.php",
	"viewAs" => URL . "Dashboard/viewAs.php",
	"resetLoginAs" => URL . "Dashboard/resetLoginAs.php",
	"logout" => URL . "Account/logout.php",
	"forget_password" => URL . "Account/",
	"change_password" => URL . "Account/",
	"profile" => URL . "Dashboard/",
	"marketplace" => URL . "MarketPlace.php",
	"createService" => URL . "Dashboard/Services/create.php",
	"removeService" => URL . "Dashboard/Services/remove.php",
	"editService" => URL . "Dashboard/Services/edit.php",
	"subscribeService" => URL . "Dashboard/Services/subscribe.php",
	"unSubscribeService" => URL . "Dashboard/Services/unsubscribe.php",
	"createPackage" => URL . "Dashboard/Packages/create.php",
	"removePackage" => URL . "Dashboard/Packages/remove.php",
	"editPackage" => URL . "Dashboard/Packages/edit.php",
	"subscribePackage" => URL . "Dashboard/Packages/subscribe.php",
	"unSubscribePackage" => URL . "Dashboard/Packages/unsubscribe.php",
	);

$GLOBALS["user_roles"] = $USER_ROLES;
$GLOBALS["urls"] = $URLS;

?>
