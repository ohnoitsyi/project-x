

<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
<LINK rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">	
<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
<TITLE>Market Place :: ProjectX</TITLE>


<?php
require("./init.php");
if ($_SESSION['user']['role'] == $GLOBALS['user_roles']['RetailCustomer']){
	$services = ServiceManager::listOnlyRetail();
} else {
	$services = ServiceManager::listAll();
	
}
$packages = PackageManager::listAll();
?>

<body class="container">
<?php require(ROOT . "div/nav.php"); ?>
<h1>List Services <small> <small><?php displayServiceTitle(); ?> </small></small></h1>

<table class="table">
<tr> <th>Name</th> <th>Description</th> <th>Type</th> <th>Rate</th> <th>Duration</th> <th>?</th>
</tr>
<?php
foreach ($services as $service){
	print "<tr> <td>{$service['name']}</td> <td>{$service['description']}</td> <td>{$service['type']}</td> <td>$ {$service['rate']}</td> <td> {$service['duration']} months</td>" . displayServiceConfig($service['id']) . "</tr>";
}

?>
</table>


<h1>List Packages <small> <small><?php displayPackageTitle(); ?> </small></small></h1>

<table class="table">
<tr> <th>Name</th> <th>Description</th>  <th>Rate</th> <th></th>
<?php //if ( isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == "MarketingRep") { echo "<th>Configure</th>"; } ?>
</tr>
<?php

foreach ($packages as $package){
	print "<tr> <td>{$package['name']}</td> <td>{$package['description']}</td> <td>$ {$package['rate']}</td> " . displayPackageConfig($package['id']) . "</tr>";
}

?>
</table>


</body>


<?php

function displayServiceTitle(){
	if ( isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == $GLOBALS["user_roles"]["MarketingRep"]) { 
		echo a($GLOBALS["urls"]['createService'],"Create New"); 
	} 
	else {
		echo "Filter By: "; 
		echo a($GLOBALS["urls"]['marketplace'], "All Services") . " . " ; 
		echo a($GLOBALS["urls"]['marketplace'] . "?filter=wiredPhone", "Wired Phone") . " . " ;
		echo a($GLOBALS["urls"]['marketplace'] . "?filter=wirelessPhone", "Wireless Phone") . " . " ;
		echo a($GLOBALS["urls"]['marketplace'] . "?filter=internet", "Internet") . " . " ;
	}
}

function displayPackageTitle(){
	if ( isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == $GLOBALS["user_roles"]["MarketingRep"]) { 
		echo a($GLOBALS["urls"]['createPackage'],"Create New"); 
	} 
}



function displayServiceConfig($id){
	if (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == $GLOBALS["user_roles"]["MarketingRep"]){
		return "<td>" . a( $GLOBALS["urls"]["editService"] . "?id={$id}", "Edit") . " | " . a( $GLOBALS["urls"]["removeService"] . "?id={$id}", "Delete", false) . "</td>";
	} /*
	else if (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == $GLOBALS["user_roles"]["Customer"]){
		return "<td>" . a("Dashboard/Subscribe.php?id={$id}&email={}", "Subscribe") . "</td>";
	}
	else if (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == "CustomerRep"){
		return "<td>" . a("Dashboard/Subscribe.php?id={$id}", "Subscribe To") . "</td>";
	} */
	
	else {
		if ($_SESSION['loginAs']){
			$email = $_SESSION['email'];
		} else {
			if (isset($_SESSION['user']['email']) && $_SESSION['user']['email']){
				$email = $_SESSION['user']['email'];
			} else {
				$email = "";
			}
		}
		return "<td>" . a($GLOBALS["urls"]["subscribeService"] . "?id={$id}&email={$email}", "Subscribe") . "</td>";
	}
}

function displayPackageConfig($id){
	if (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == $GLOBALS["user_roles"]["MarketingRep"]){
		return "<td>" . a( $GLOBALS["urls"]["editPackage"] . "?id={$id}", "Edit") . " | " . a( $GLOBALS["urls"]["removePackage"] . "?id={$id}", "Delete", false) . "</td>";
	} else {
		if ($_SESSION['loginAs']){
			$email = $_SESSION['email'];
		} else {
			if (isset($_SESSION['user']['email']) && $_SESSION['user']['email']){
				$email = $_SESSION['user']['email'];
			} else {
				$email = "";
			}
		}
		return "<td>" . a($GLOBALS["urls"]["subscribePackage"] . "?id={$id}&email={$email}", "Subscribe") . "</td>";
	}
	/*
	if (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == "MarketingRep"){
		return "<td>" . a("Dashboard/Packages/edit.php?id={$id}", "Edit") . " | " . a("Dashboard//Packages/Delete.php?id={$id}", "Delete") . "</td>";
	} 
	else if (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == "Customer"){
		return "<td>" . a("Dashboard/Packages/subscribe.php?id={$id}", "Subscribe") . "</td>";
	}
	else if (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == "CustomerRep"){
		return "<td>" . a("Dashboard/Packages/subscribe.php?id={$id}", "Subscribe To") . "</td>";
	}  else {
		return "<td>" . a("Dashboard/Packages/subscribe.php?id={$id}", "Subscribe") . "</td>";
	}*/
}

?>