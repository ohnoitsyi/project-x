<?php

define("ROOT", "/Applications/XAMPP/htdocs/teleport/");

require(ROOT. "config.php");
require(ROOT . "models/DB.php");
require(ROOT . "lib/Helper.php");
require(ROOT . "manager/UserManager.php");
require(ROOT . "models/Users.php");
require(ROOT . "models/Admin.php");
require(ROOT . "models/CustomerRep.php");
//require(ROOT . "models/Users.php");
require(ROOT . "models/Product.php");
require(ROOT . "manager/ServiceManager.php");
require(ROOT . "models/Services.php");
require(ROOT . "models/Customer.php");
require(ROOT . "manager/packageManager.php");
require(ROOT . "models/Packages.php");


//$user = new User();

session_start();
	
if ( !isset($_SESSION['loginAs'])){
	$_SESSION['loginAs'] = 0;	
}
/*
if (!isset($_SESSION['email']) &&  isset($_SESSION['user']['email']) ){ // UserManager::isLogin()
	$_SESSION['email'] = $_SESSION['user']['email'];
}
*/
if (isset($_SESSION['user']) && is_array($_SESSION['user'])){
	if ($_SESSION['user']['role'] == "Admin"){
		$user = new Admin($_SESSION['user']['email']);
	} 
	else if ($_SESSION['user']['role'] == $GLOBALS['user_roles']["CustomerRep"]){
		$user = new CustomerRep($_SESSION['user']['email']);
	}
	else if ($_SESSION['user']['role'] == $GLOBALS['user_roles']["RetailCustomer"]){
		$user = new Customer($_SESSION['user']['email']);
	}
	else if ($_SESSION['user']['role'] == "Customer"){
		$user = new Customer($_SESSION['user']['email']);
	} else {
		$user = new User(0);
	}
}

$GLOBALS['user'] = $user;



?>