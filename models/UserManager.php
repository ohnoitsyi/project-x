<?php

class UserManager{
	
	static function login(){ // May be static
		if (isset($_POST['email']) && isset($_POST['password'])){
			//$input = array(md5($_POST['email']), md5($_POST['password'])); // We will care about salt later.
			$input = array($_POST['email'], md5($_POST['password']));
			// DB Connect
			$query = $this->DB->prepare("SELECT name, email, role FROM users WHERE email=? AND password=?");
			$result = $query->execute($input);
			//$result = query('SELECT name, email FROM users WHERE ');
			$result = $query->fetch(); //PDO::FETCH_ASSOC);
			//print_r($result);
				
			if (!$result){
				// 
				//$_SESSION['errMsg'] = 'ERROR: Invalid Email or Password. <a href="/ProjectX/account.php?do=login">Try again</a>';
				$_SESSION["msg"] = array("str" => "Invalid Email or Password.", "status" => 1);
				//header("Location: account.php?do=login");
				//exit();
				redirect("Dashboard/?for=signup_success");
			} else {
				// Successfully Login
				$_SESSION['user']['login'] = 1;
				$_SESSION['user']['name'] = $result['name'];
				$_SESSION['user']['email'] = $result['email'];
				$_SESSION['user']['role'] = $result['role'];

				//print_r($_SESSION);
				//$this->displaySuccess("You have successfully Login with " . $_SESSION['user']['email'] . ' <a href="/ProjectX/account.php?do=logout">Logout</a>');
				//header("Location: profile.php");
				//exit();	
				redirect("Dashboard/");
			}

		}
		else {
			//$_SESSION['errMsg'] = 'ERROR: Something went wrong. Missing Email or Password or Wrong Email or Password. <a href="/ProjectX/account.php?do=login">Try again</a>';
			$_SESSION["msg"] = array("str" => "Something went wrong. Missing Email or Password or Wrong Email or Password.", "status" => 1);
			//header("Location: account.php?do=login&for=form_error");
			//exit();
			redirect("Account/?do=login&for=form_error");
		}
	}

	function logout(){
		session_unset();
		session_destroy();
		session_start();
		$_SESSION["msg"] = array("str" => "Successfully Logout!", "status" => 0);
		//header("Location: account.php");
		//exit();
		redirect("Account/");
	}
}

?>