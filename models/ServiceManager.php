<?php

class ServiceManager{
	static function add($name, $description, $type, $rate){
		$str = "INSERT INTO `services`(`name`, `description`, `type`, `rate`) VALUES (?,?,?,?)";						
		$input = array($name, $description, $type, $rate);
		DB::run_exec($str, $input);
	}
	static function listAll(){
		$DB = new DB();
		$str = "SELECT id, name, description, type, rate FROM services";
		return $DB->query($str);
	}
	static function delete($service){
		$DB = new DB();
		$str = "DELETE FROM services WHERE id=?";
		//$what = array($services->getID());
		$DB->exec($str, array($service));
	}
}
?>
	