<?php

//define("ROOT", "/Applications/XAMPP/htdocs/ProjectX/");
//require(ROOT . "init.php");
//init();

class Customer extends User{

	function getThreshold(){  
		return $this->attrib['threshold'];
	}

	function getAmmount(){  
		return $this->attrib['ammount'];
	}
	
	static function pay($email, $ammount){
		$str = "SELECT ammount FROM users WHERE email=?";
		$opt = array($email);
		$result = DB::run_query($str, $opt);
		$now = $result[0]['ammount'] - $ammount;
		// ===

		$str = "UPDATE users SET ammount=? WHERE email=?";
		$opt = array($now, $email);
		DB::run_exec($str, $opt);
		$_SESSION['msg']['str'] = "Successfully Pay $" . $ammount . " . Now your ammont is " . $now . ".";
		$_SESSION['msg']['status'] = 0;
		//print $_SESSION['msg']['str'];
	}
	static function subscribePackage($email, $package){
		// get the Services he subscribed
		$DB = new DB();
		$str = "SELECT packages FROM users WHERE email=?";						
		$input = array($email);
		$result = $DB->query($str, $input);
		if  ($result == 0){ 
			$packages = array(); 
		}
		else {
			$packages = unserialize($result[0]['packages']); 
			if( !is_array($packages)){ $packages = array($packages); }
		}

		// set the Service he subscribed

		if (!in_array($package, $packages)){
			$packages[] = $package;

			
		}
		print "---";
		$packages = serialize($packages);
		$str = "UPDATE users SET packages=? WHERE email=?";						
		$input = array($packages, $email);
		$DB->exec($str, $input);		
	}

	static function unSubscribePackage($email, $package){
		$total = 0;
		$DB = new DB();
		$str = "SELECT packages, ammount FROM users WHERE email=?";						
		$input = array($email);
		$result = $DB->query($str, $input);
		if  ($result == 0){ 
			$packages = array(); 
		}
		else {
			$packages = unserialize($result[0]['packages']); 
			if( !is_array($packages)){
				$packages = array($packages); 
			}
		}

		$total = $result[0]['ammount'];
		// set the Service he subscribed
		if (in_array($package, $packages)){
			// removing
			if(($key = array_search($package, $packages)) !== false) {
			    unset($packages[$key]);
			}
			// end of remove
			//$services[] = $service;
			$objPackage = new Package($package);
			$total = $total - $objPackage->getRate();
		}
		$packages = serialize($packages);
		$str = "UPDATE users SET packages=? WHERE email=?";						
		$input = array($packages, $email);
		$DB->exec($str, $input);
	}

	static function subscribe($email, $service){
		// get the Services he subscribed
		$total = 0;
		$DB = new DB();
		$str = "SELECT services, ammount FROM users WHERE email=?";						
		$input = array($email);
		$result = $DB->query($str, $input);
		if  ($result == 0){ 
			$services = array(); 
		}
		else {
			//echo "bit";
			
			$services = unserialize($result[0]['services']); 
			if( !is_array($services)){$services = array($services); }
		}

		$total = $result[0]['ammount'];
		print "before " . $total;	
		// set the Service he subscribed
		if (!in_array($service, $services)){
			$services[] = $service;
			$objService = new Service($service);
			$total = $total + $objService->getRate();
		}
		$services = serialize($services);
		//print $services;
		$str = "UPDATE users SET services=?, ammount=? WHERE email=?";						
		$input = array($services, $total ,$email);
		$DB->exec($str, $input);
		
	}


	


	static function unSubscribe($email, $service){
		$DB = new DB();
		$str = "SELECT services, ammount FROM users WHERE email=?";						
		$input = array($email);
		$result = $DB->query($str, $input);
		if  ($result == 0){ 
			$services = array(); 
		}
		else {
			$services = unserialize($result[0]['services']); 
			if( !is_array($services)){$services = array($services); }
		}
		$total = $result[0]['ammount'];
		// set the Service he subscribed
		if (in_array($service, $services)){
			//print_r($services);
			// removing
			if(($key = array_search($service, $services)) !== false) {
			    unset($services[$key]);
			}
			print $service;
			$objService = new Service($service);
			$total = $total - $objService->getRate();
			// end of remove
			//$services[] = $service;
		}
		$services = serialize($services);
		$str = "UPDATE users SET services=?, ammount=? WHERE email=?";						
		$input = array($services, $total, $email);
		$DB->exec($str, $input);
	}
	static function listSubscribe($email){
		$DB = new DB();
		$str = "SELECT services FROM users WHERE email=?";
		//print($email);
		$result = $DB->query($str, array($email));
		//print_r($result);
		//print("<br>");
		//print(($result[0]["service"]));
		if ($result == 0){ return 0; }
		else {
		//print_r(unserialize($result[0]["service"]));
		return unserialize($result[0]["services"]); }
	}

	static function listSubscribePackages($email){
		$DB = new DB();
		$str = "SELECT packages FROM users WHERE email=?";
		//print($email);
		$result = $DB->query($str, array($email));
		//print_r($result);
		//print("<br>");
		//print(($result[0]["service"]));
		if ($result == 0){ return 0; }
		else {
		//print_r(unserialize($result[0]["service"]));
		return unserialize($result[0]["packages"]); }
	}

}

?>
