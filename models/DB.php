<?php

/**
 * Copy from web.php and Edited
 * 
 */
class DB{
	private $host;
	private $dbname;
	private $user;
	private $password;
	private $DB;

	function __construct(){

		$this->host = DB_HOST;
		$this->dbname = DB_NAME;
		$this->user = DB_USER;
		$this->password = DB_PASSWORD;
		//$this->DB = new PDO('mysql:host=localhost;dbname=proj_x;charset=utf8', 'root', '');
		try{
			$this->DB = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASSWORD);	
		} catch (PDOException $e) {
	        print "Error: " . $e->getMessage() . "<br/>";
	        exit();
		}
		
	}

	function exec($str, $var = array()){
		try{
			$query = $this->DB->prepare($str);
			$result = $query->execute($var);
		} catch (PDOException $e) {
	    	print "Error: " . $e->getMessage() . "<br/>";
	    	exit();
		}
		
	}

	function query($str, $var = array()){
		try {
			$query = $this->DB->prepare($str);
			$query->execute($var);
			return $query->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
	        print "Error: " . $e->getMessage() . "<br/>";
	        exit();
		}
	}

	static function run_exec($str, $var = array()){
		$db = new DB();
		$db->exec($str, $var);
	}
	static function run_query($str, $var = array()){
		$db = new DB();
		return $db->query($str, $var);
	}
}

?>