<?php

class Package implements Product {
	// 
	/*
	DB:: Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, 
		name TEXT, 
		description TEXT, 
		services TEXT, // array(__services__)
		rate DOUBLE
	*/
	private $id;
	private $title;
	private $description;
	private $services;
	private $rate;

	function __construct($id = 0){
		//$this->DB = new DB();
		//$this->DB->exec("CREATE TABLE services(Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, name TEXT, description TEXT, type TEXT, rate DECIMAL)");
		//$this->id = $id;
		if ($id != 0){
			$str = "SELECT id, name, description, services, rate FROM packages WHERE id=?";
			$result = DB::run_query($str, array($id));
			//print_r($result);
			$result = $result[0];
			//print "====";
			//print_r($result);
			$this->id = $result['id'];
			$this->title = $result['name'];
			$this->description = $result['description'];
			$this->services = unserialize($result['services']);
			$this->rate = $result['rate'];
		}
	}

	function getID(){ return $this->id; }

	function getTitle(){ return $this->title; }
	function setTitle($title){ $this->title = $title; }
	
	function setDescription($description){ $this->description = $description; }
	function getDescription(){ return $this->description; }

	function setRate($rate){ $this->rate = $rate; }
	function getRate(){ return $this->rate; }

	function getServices(){
		return $this->services;
	}
	
	function display(){
		if ($this->title == ""){ return ""; }
		$email = "";
		return " &raquo; <b>Package::" . $this->title . "</b> with monthly rate $" .  $this->rate . " ". a("/Dashboard/UnSubscribe.php?id=" . $this->id . "&email=" . $email, "Unsuscribe") . "<br>";
	}

	
	
}


?>