<?php

interface Product{
	public function getID();

	public function getTitle();
	
	public function setDescription($description);
	public function getDescription();

	public function setRate($rate);
	public function getRate();
}

?>