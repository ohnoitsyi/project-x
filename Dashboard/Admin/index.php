<!DOCTYPE html>
<HTML>
	<HEAD>
		<META charset="utf-8">
		<TITLE>Account :: ProjectX</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<BODY class="container">
<?php

require("../../init.php");
require(ROOT. "div/nav.php");

?>


<?php

echo "<h3> List of Customers <small><a href=\"{$GLOBALS['urls']['signup']}\">Create New User</a></small></h3>";
$result = UserManager::listAll();
echo '<table class="table ">';
foreach ($result as $v){
	print "<tr> <td> <b>{$v['name']}</b> </td> <td>{$v['email']} </td> <td>{$v['role']}</td> <td><a href=\"{$GLOBALS['urls']['admin']}?email={$v['email']}\">Manage</a> </td></tr>";
}
echo "</table>";
?>