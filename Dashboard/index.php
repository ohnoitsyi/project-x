<!DOCTYPE html>
<HTML>
	<HEAD>
		<META charset="utf-8">
		<TITLE>Account :: ProjectX</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<BODY class="container">
<?php

require("../init.php");
require(ROOT. "div/nav.php");

if (!UserManager::isLogin()){
	redirect($GLOBALS["urls"]["login"]);
}

echo '<div class="container">';
echo "<h1> Hello {$_SESSION['user']['name']} ({$_SESSION['user']['role']})</h1>";

echo displayMsg();
if ($_SESSION['user']['role'] == $GLOBALS['user_roles']['Admin']){
	echo '<h3><a href="">Manage User</a> </h3><br>';
}
else if ($_SESSION['user']['role'] == $GLOBALS['user_roles']['MarketingRep']){
	echo "<h3>";
	echo ' --- <a href="' . $GLOBALS['urls']['marketplace'] . '">Go To MarketPlace</a> --- <br>';
	echo " Services : ";
	echo '<a href="' . $GLOBALS['urls']['createService'] . '">Create</a>';
	//echo '<a href="' . $GLOBALS['urls']['removeService'] . '">Remove</a>';
	//echo '<a href="' . $GLOBALS['urls']['editService'] . '">Edit</a>';
	echo "<br>";
	echo " Packages : ";
	echo '<a href="' . $GLOBALS['urls']['createPackage'] . '">Create</a>';
	//echo '<a href="">Remove</a>';
	echo '<a href="' . $GLOBALS['urls']['editPackage'] . '">Edit</a>';
	echo "</h3>";

}
else if ($_SESSION['user']['role'] == $GLOBALS['user_roles']['CustomerRep']){
	if ($_SESSION['loginAs']){
		echo "<h4> Currently managing User:{$_SESSION['email']} - <a href=\"{$GLOBALS['urls']['resetLoginAs']}\">Reset</a></h4>";
		redirect($GLOBALS['urls']['viewAs']);
	}
	echo "<h3> List of Customers <small><a href=\"{$GLOBALS['urls']['signup']}\">Create New User</a></small></h3>";
	$result = UserManager::listCustomer();
	echo '<table class="table ">';
	foreach ($result as $v){
		//if ($v['role'] != "Customer"){ continue; }
		print "<tr> <td> <b>{$v['name']}</b> </td> <td>{$v['email']} </td> <td>{$v['role']}</td> <td><a href=\"{$GLOBALS['urls']['loginAs']}?email={$v['email']}\">Manage</a> </td></tr>";
	}
	echo "</table>";
}
else if ($_SESSION['user']['role'] == $GLOBALS['user_roles']['RetailCustomer']){
	//echo '<h3><a href="">Manage User</a> </h3><br>';
	customerView();
}
else if ($_SESSION['user']['role'] == $GLOBALS['user_roles']['Customer']){
	//echo "<h3>Billing</h3>";
	customerView();
} else {
	// no such case
}
echo "</div>";
?>
<style>
	h3 a { display: inline-block; margin: 0px 10px;}
</style>
</BODY>
</HTML>

<?php

function customerView(){
	// ====== Services ===
	echo "<h3>";
	if ( $GLOBALS['user']->getThreshold() > 0){
		echo "Current threshold $" . $GLOBALS['user']->getThreshold() . " | <a href=\"{$GLOBALS['urls']['setThreshold']}\">Change Threshold</a>";
	} else {
		echo "<a href=\"{$GLOBALS['urls']['setThreshold']}\">Set Threshold</a>";	
	}
	echo "<br>Your current Due : \${$GLOBALS['user']->getAmmount()} | <a href=\"{$GLOBALS['urls']['payBill']}\">Pay Bill</a></h3>";
	echo "<h3>Subscribed Services:</h3>";
	$result = Customer::listSubscribe($_SESSION['user']['email']);
	//$total = 0;
	if (is_array($result)  && $result != array()){
		//foreach (Customer::listSubscribe($_SESSION['user']['email']) as $v){
		foreach ($result as $v){
			$s = new Service($v);
			if ($s->getTitle() == ''){ continue; }
			//print $s->display($_SESSION['user']['email']);
			$email = $_SESSION['user']['email'];
			echo " &raquo; <b>" . $s->getTitle() . "</b> with monthly rate $" .  $s->getRate() . " for <i>" . $s->getType() . "</i> ". a($GLOBALS["urls"]["unSubscribeService"] . "?id=" . $s->getID() . "&email=" . $email , "Unsuscribe") . "<br>";
			//$total = $total + $s->getRate() ;
		}
	} else {
		echo "None !";
	}

	print "<br> --- :) --- :) --- <br>";
	
	// ====== Packages  ===
	echo "<h3>Subscribed Packages:</h3>";
	$result = Customer::listSubscribePackages($_SESSION['user']['email']);
	if (is_array($result) && $result != array()){
		//foreach (Customer::listSubscribePackages($_SESSION['user']['email']) as $v){
		foreach ($result as $v){
			$package = new Package($v);
			if ($package->getTitle() == ''){ continue; }
			$email = $_SESSION['user']['email'];
			echo " &raquo; <b>Package::" . $package->getTitle() . " ( " . $package->getDescription() . " ) ";
			echo a($GLOBALS["urls"]["unSubscribePackage"] . "?id=" . $package->getID() . "&email=" . $email , "Unsuscribe");
			echo "</b> <br>with monthly rate $" . $package->getRate();
			if (is_array($package->getServices())){
				echo " Services include ";
				foreach ($package->getServices() as $s){
					$service = new Service($s);
					echo $service->getTitle() . " . ";
				} 
			}
			echo "<br><br>";
			//$total = $total + $package->getRate() ;
		}
	} else {
		echo "None !";
	}
	//echo "<h3>Total Bill Due: $" . $total . "</h3>"; 
}
?>