<!DOCTYPE html>
<HTML>
	<HEAD>
		<META charset="utf-8">
		<TITLE>Account :: ProjectX</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<BODY class="container">
<?php

require("../init.php");
require(ROOT. "div/nav.php");
echo '<div class="container">';
if ($_SESSION['loginAs']){
	echo "<h4> Currently managing User:{$_SESSION['email']} - <a href=\"{$GLOBALS['urls']['resetLoginAs']}\">Reset</a></h4>";
}

customerView();

echo "</div>";
?>
<style>
	h3 a { display: inline-block; margin: 0px 10px;}
</style>
</BODY>
</HTML>
<?

function customerView(){
	// ====== Services ===
	echo "<h3>Subscribed Services:</h3>";
	$result = Customer::listSubscribe($_SESSION['email']);
	$total = 0;
	if (is_array($result)  && $result != array()){
		//foreach (Customer::listSubscribe($_SESSION['user']['email']) as $v){
		foreach ($result as $v){
			$s = new Service($v);
			if ($s->getTitle() == ''){ continue; }
			//print $s->display($_SESSION['user']['email']);
			$email = $_SESSION['email'];
			echo " &raquo; <b>" . $s->getTitle() . "</b> with monthly rate $" .  $s->getRate() . " for <i>" . $s->getType() . "</i> ". a($GLOBALS["urls"]["unSubscribeService"] . "?id=" . $s->getID() . "&email=" . $email , "Unsuscribe") . "<br>";
			$total = $total + $s->getRate() ;
		}
	} else {
		echo "None !";
	}

	print "<br> --- :) --- :) --- <br>";
	
	// ====== Packages  ===
	echo "<h3>Subscribed Packages:</h3>";
	$result = Customer::listSubscribePackages($_SESSION['email']);
	if (is_array($result) && $result != array()){
		foreach ($result as $v){
			$package = new Package($v);
			if ($package->getTitle() == ''){ continue; }
			$email = $_SESSION['email'];
			echo " &raquo; <b>Package::" . $package->getTitle() . " ( " . $package->getDescription() . " ) ";
			echo a($GLOBALS["urls"]["unSubscribePackage"] . "?id=" . $package->getID() . "&email=" . $email , "Unsuscribe");
			echo "</b> <br>with monthly rate $" . $package->getRate();
			if (is_array($package->getServices())){
				echo " Services include ";
				foreach ($package->getServices() as $s){
					$service = new Service($s);
					echo $service->getTitle() . " . ";
				} 
			}
			echo "<br><br>";
			$total = $total + $package->getRate() ;
		}
	} else {
		echo "None !";
	}
	echo "<h3>Total Bill Due: $" . $total . "</h3>"; 
}
?>