<?php

// index will list the package
// create() = create.php
// remove() = remove.php
// edit() = edit.php

require("../../init.php");

/*
if (!($_SESSION['user']['role'] == "MarketingRep")){
	$_SESSION['msg']['str'] = "Invalid User assessing the fectures. If you think, this is somrthing wrong contact webmaster.";
	$_SESSION['msg']['status'] = 1;
	redirect("404.php");
} else {
	// So login as Marketing rep */
	if (isset($_POST['edit']) && $_POST['edit'] == 1){
		//print_r($_POST['services']);
		//print(serialize($_POST['services']));
		PackageManager::edit($_POST['id'], $_POST['title'], $_POST['description'], $_POST['services'], $_POST['rate']);
		$_SESSION['msg']['str'] = 'Package Successfully Updated. Return to ' . a($GLOBALS['urls']['marketplace'], "MarketPlace");
		$_SESSION['msg']['status'] = 0;
	}
//}
// */

$package = new Package($_GET['id']);

?>


<html>
<HEAD>
		<META charset="utf-8">
		<TITLE>Editing Packages</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<body class="container"> 

		
		<?php require(ROOT . "div/nav.php"); ?>
<section id="signup">
	<h1 class="center">Editing Packages:: <?php echo $package->getTitle(); ?>
		<small>
			<a href="index.php"> View Packages </a>
		</small>
	</h1>
	<?php echo displayMsg(); ?>

	<form method="post" class="form">
	<div class="form-group">
		<label for="name">Package Name: </label>
		<input type="text" name="title" class="form-control" value="<?php echo $package->getTitle(); ?>" required>
	</div>
	<div class="form-group">
		<label for="description">Description of Package: </label>
		<textarea name="description" class="form-control" rows="4" required><?php echo $package->getDescription(); ?></textarea>
	</div>
	<div class="form-group">
		<label for="rate">Rate of Package (per month): </label>
		<input type="number" value="<?php echo $package->getRate(); ?>" name="rate" class="form-control" required>
	</div>
	<input type=text name="id" value="<?php echo $_GET['id']; ?>" hidden >
	<div class="form-group">
		
		<?php
		$all = ServiceManager::listAll();
		$added = $package->getServices();
		?>
		<label for="description">Already Added Services: </label>
		<?php
		foreach ($added as $s){
			$service = new Service($s); 
		?>
			<input type="checkbox" name="services[]" value="<?php echo $service->getID();?>" checked>  <?php echo $service->getTitle(); ?>&nbsp;
		<?	
		}
		?>
		<br>
		<label for="description">Other Services:</label>
		<!--select class="form-control" name="type" required -->
			<?php foreach ($all as $row){ 
				if (in_array($row['id'], $added)){ continue; }
			?>

			<!--option value="<?php echo $row['id'];?>"><?php echo $row['name']; ?></option-->
			<input type="checkbox" name="services[]" value="<?php echo $row['id'];?>">  <?php echo $row['name']; ?>&nbsp;
			<?php } ?>
		<!-- /select -->
	</div>
	<button type="submit" class="btn btn-default pull-right" name="edit" value="1">Edit Package</button>
	<!-- input type="submit" name="add" class="btn btn-default pull-right" value="Create Service" -->
</form>


</section>
</body>
</html>