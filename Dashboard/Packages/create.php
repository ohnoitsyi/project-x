<?php

require("../../init.php");

/*
if (!($_SESSION['user']['role'] == "MarketingRep")){
	$_SESSION['msg']['str'] = "Invalid User assessing the fectures. If you think, this is somrthing wrong contact webmaster.";
	$_SESSION['msg']['status'] = 1;
	redirect("404.php");
} else {
	// So login as Marketing rep */
	if (isset($_POST['create']) && $_POST['create'] == 1){
		//print_r($_POST['services']);
		//print(serialize($_POST['services']));
		PackageManager::add($_POST['title'], $_POST['description'], $_POST['services'], $_POST['rate']);
		$_SESSION['msg']['str'] = 'Services Successfully Created. Return to ' . a($GLOBALS['urls']['marketplace'], "MarketPlace");
		$_SESSION['msg']['status'] = 0;
	}

?>


<html>
<HEAD>
		<META charset="utf-8">
		<TITLE>Create Packages</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<body class="container"> 

		
		<?php require(ROOT . "div/nav.php"); ?>
<section id="signup">
	<h1 class="center">Create Packages
		<small>
			<a href="index.php"> View Packages </a>
		</small>
	</h1>
	<?php echo displayMsg(); ?>

	<form method="post" class="form">
	<div class="form-group">
		<label for="name">Package Name: </label>
		<input type="text" name="title" class="form-control" required>
	</div>
	<div class="form-group">
		<label for="description">Description of Package: </label>
		<textarea name="description" class="form-control" rows="4" required></textarea>
	</div>
	<div class="form-group">
		<label for="rate">Rate of Package (per month): </label>
		<input type="number" name="rate" class="form-control" required>
	</div>
	<div class="form-group">
		<label for="description">Initial service to add: </label>
		<?php
		$result = ServiceManager::listAll();
		?>
		<!--select class="form-control" name="type" required -->
			<?php foreach ($result as $row){ ?>
			<!--option value="<?php echo $row['id'];?>"><?php echo $row['name']; ?></option-->
			<input type="checkbox" name="services[]" value="<?php echo $row['id'];?>">  <?php echo $row['name']; ?>&nbsp;
			<?php } ?>
		<!-- /select -->
	</select>
	</div>
	<button type="submit" class="btn btn-default pull-right" name="create" value="1">Create Package</button>
	<!-- input type="submit" name="add" class="btn btn-default pull-right" value="Create Service" -->
</form>


</section>
</body>
</html>