<?php

// index will list the package
// create() = create.php
// remove() = remove.php
// edit() = edit.php

define("ROOT", "/Applications/XAMPP/htdocs/ProjectX/");
require(ROOT . "init.php");
init();


?>


<html>
<HEAD>
		<META charset="utf-8">
		<TITLE>View Package :: ProjectX</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<body class="container"> 

		
		<?php require(ROOT . "div/user_nav.php"); ?>
<section id="View Packages">
	<h1 class="center">View Packages
		<small>
			<a href="create.php"> Create New </a>
		</small>
	</h1>
	<?php echo displayMsg(); ?>

	<!-- form method="post" class="form">
	<div class="form-group">
		<label for="name">Package Name: </label>
		<input type="text" name="title" class="form-control" required>
	</div>
	<div class="form-group">
		<label for="description">Description of Package: </label>
		<textarea name="description" class="form-control" rows="4" id="comment" required></textarea>
	</div>
	<div class="form-group">
		<label for="rate">Rate of Package (per month): </label>
		<input type="number" name="rate" class="form-control" id="comment" required>
	</div>
	<div class="form-group">
		<label for="description">Initial service to add: </label>
		<select class="form-control" name="type" required>
			<option value="1">Need to link to MySQL</option>
			<option value="2">Finish that tonight</option>
			<option value="3">Getting it to work</option>
	</select>
	</div>
	<button type="submit" class="btn btn-default pull-right" name="add" value="1">Create Package</button>
	<!-- input type="submit" name="add" class="btn btn-default pull-right" value="Create Service" -- >
<!-- /form -->
<?php 
$result = Package::listAll();
//print("---[" . $result . "]---");
foreach ($result as $row){
	$package = new Package($row['id']);
	//echo $package->display();
	echo " &raquo; <b>Package::" . $package->getTitle() . " ( " . $package->getDescription() . " ) ";
	echo ' <a href="edit.php?do=edit&id=' . $row['id'] . '">Edit </a>';
	echo ' <a href="remove.php?do=remove&id=' . $row['id'] . '">Delete </a><br>';
	echo "</b> with monthly rate $" . $package->getRate();
	if (is_array($package->getServices())){
	echo " Services include ";
		foreach ($package->getServices() as $s){
		$service = new Service($s);
		echo $service->getTitle() . " . ";
		} 
		
	}
	echo "<br><br>";
}
?>

</section>
</body>
</html>