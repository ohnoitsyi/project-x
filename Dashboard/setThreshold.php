<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
<LINK rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">	
<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>


<?php

// set threshold
require("../init.php");

require(ROOT . "div/nav.php");

if (isset($_GET['ts']) && $_GET['ts'] != ""){
	$str = "UPDATE users SET threshold=? WHERE email=?";
	$opt = array($_GET['ts'], $_SESSION['user']['email']);
	DB::run_exec($str, $opt);
	$_SESSION["msg"] = array("str" => "Successfully set threshold", "status" => 0);
	redirect($GLOBALS['urls']['profile']);
}

?>


<div class="text-center">
	<form class="form-inline">
		<div class="form-group">
			<label>Set Threshold: </label>
			<input type="number" name="ts" class="form-control" require>
			<button type="submit" class="btn btn-default">Go</button>
		</div>
	</form>
</div>
