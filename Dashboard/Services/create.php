<?php

require("../../init.php");

/*
if (!($_SESSION['user']['role'] == "MarketingRep")){
	$_SESSION['msg']['str'] = "Invalid User assessing the fectures. If you think, this is somrthing wrong contact webmaster.";
	$_SESSION['msg']['status'] = 1;
	redirect("404.php");
} 
*/

// So login as Marketing rep
if (isset($_POST['add']) && $_POST['add'] == 1){
	ServiceManager::add($_POST['title'], $_POST['description'], $_POST['type'], $_POST['rate'], $_POST['duration'], $_POST['retail']);
	$_SESSION['msg']['str'] = 'Services Successfully Created. Return to ' . a($GLOBALS['urls']['marketplace'], "MarketPlace");
	$_SESSION['msg']['status'] = 0;
}

?>

<html>
<HEAD>
		<META charset="utf-8">
		<TITLE>Add Service</TITLE>
		<SCRIPT src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<SCRIPT src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="assets/style.css">
	</HEAD>
	<body class="container"> 

		<?php require(ROOT . "div/nav.php"); ?>
		<section id="create">
			<h1 class="center">Add Service</h1>
			<?php echo displayMsg(); ?>

			<form method="post" class="form">
				<div class="form-group">
					<label for="name">Title of Service: </label>
					<input type="text" name="title" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="description">Description: </label>
					<textarea name="description" class="form-control" rows="4" required></textarea>
				</div>
				<div class="form-group">
					<label for="rate">Rate (per month): </label>
					<input type="number" name="rate" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="type">Type: </label>
					<input type="radio" name="type" value="WiredPhone" checked> Wired Phone 
					<input type="radio" name="type" value="WirelessPhone" checked> Wireless Phone 
					<input type="radio" name="type" value="Internet" checked> Internet 
				</div>
				<div class="form-group">
					<label for="type">Retail ? : </label>
					<input type="radio" name="retail" value="1" checked> Yes
					<input type="radio" name="retail" value="0" checked> No
				</div>
				<div class="form-group">
					<label for="duration">Duration: </label>
					<input type="number" name="duration" class="form-control" required>
				</div>
				<button type="submit" class="btn btn-default pull-right" name="add" value="1">Create Service</button>
			</form>

		</section>
	</body>
</html>

